package com.jpawsdl.starter.jpa.todo;

import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "Todo")
final class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "creation_time", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.threeten.PersistentZonedDateTime")
    private ZonedDateTime creationTime;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "modificationTime")
    @Type(type = "org.jadira.usertype.dateandtime.threeten.PersistentZonedDateTimeAsStringAndStringZone")
    private ZonedDateTime modificationTime;

    @Column(name = "title", nullable = false, length = 100)
    private String title;

    @Version
    private Long version;

    public Todo() {
    }

    public Todo(Long id, ZonedDateTime creationTime, String description, ZonedDateTime modificationTime, String title,
            Long version) {
        this.id = id;
        this.creationTime = creationTime;
        this.description = description;
        this.modificationTime = modificationTime;
        this.title = title;
        this.version = version;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreationTime() {
        return this.creationTime;
    }

    public void setCreationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getModificationTime() {
        return this.modificationTime;
    }

    public void setModificationTime(ZonedDateTime modificationTime) {
        this.modificationTime = modificationTime;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Todo id(Long id) {
        this.id = id;
        return this;
    }

    public Todo creationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public Todo description(String description) {
        this.description = description;
        return this;
    }

    public Todo modificationTime(ZonedDateTime modificationTime) {
        this.modificationTime = modificationTime;
        return this;
    }

    public Todo title(String title) {
        this.title = title;
        return this;
    }

    public Todo version(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Todo)) {
            return false;
        }
        Todo todo = (Todo) o;
        return Objects.equals(id, todo.id) && Objects.equals(creationTime, todo.creationTime)
                && Objects.equals(description, todo.description)
                && Objects.equals(modificationTime, todo.modificationTime) && Objects.equals(title, todo.title)
                && Objects.equals(version, todo.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creationTime, description, modificationTime, title, version);
    }

    @Override
    public String toString() {
        return "{" + " id='" + getId() + "'" + ", creationTime='" + getCreationTime() + "'" + ", description='"
                + getDescription() + "'" + ", modificationTime='" + getModificationTime() + "'" + ", title='"
                + getTitle() + "'" + ", version='" + getVersion() + "'" + "}";
    }

}