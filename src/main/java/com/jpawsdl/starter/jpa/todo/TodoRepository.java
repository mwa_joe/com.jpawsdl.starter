package com.jpawsdl.starter.jpa.todo;

import org.springframework.data.repository.CrudRepository;

interface TodoRepository extends CrudRepository<Todo, Long> {
}